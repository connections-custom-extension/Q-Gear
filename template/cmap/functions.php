<?php
namespace Connections_Directory\Template\cMap\Q_Gear;

use cnArray;
use cnMeta;
use cnOutput;
use Connections_QGear;

/**
 * @param cnOutput $entry
 */
function renderTwitterHandle( $entry ) {

	$meta          = cnMeta::get( 'entry', $entry->getId(), 'twitter', TRUE );
	$twitterURL    = $meta;
	$twitterHandle = '';

	if ( is_string( $twitterURL ) && 0 < strlen( $twitterURL ) ) {

		preg_match( '/http(?:s):\\/\\/twitter.com\\/(?:#!\\/)?([^\\/]*)\\/?/', $twitterURL, $matches );

		if ( array_key_exists( 1, $matches ) ) {

			$twitterHandle = $matches[1];
		}
	}

	if ( 0 < strlen( $twitterHandle ) ) {

		printf(
			'<div><strong>Twitter:</strong> @%s</div>',
			$twitterHandle
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderYouTubeName( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'youtube-main|name', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<div><strong>YouTube:</strong> %s</div>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderParlerName( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'parler', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<div><strong>Parler:</strong> %s</div>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderQStance( $entry ) {

	//$meta   = cnMeta::get( 'entry', $entry->getId(), 'q_stance', TRUE );
	//$stance = Connections_QGear::qStanceOptions( $meta );
	//
	//if ( is_string( $stance ) && 0 < strlen( $stance ) ) {
	//
	//	printf(
	//		'<div><strong>Q Stance:</strong> %s</div>',
	//		$stance
	//	);
	//}

	$terms = $entry->getCategory( array( 'child_of' => 120 ) );

	if ( ! is_array( $terms ) || empty( $terms ) ) {

		return;
	}

	$terms = wp_list_pluck( $terms, 'name', 'term_id' );

	printf(
		'<div><strong>Q Stance:</strong> %s</div>',
		implode( ', ', $terms )
	);
}

/**
 * @param cnOutput $entry
 */
function renderFocus( $entry ) {

	//$value = array();
	//$meta  = cnMeta::get( 'entry', $entry->getId(), 'focus', TRUE );
	//
	//if ( ! empty( $meta ) ) {
	//
	//	if ( is_array( $meta ) ) {
	//
	//		foreach ( $meta as $key ) {
	//
	//			$option = Connections_QGear::focusOptions( $key );
	//
	//			if ( FALSE !== $option ) {
	//
	//				array_push( $value, $option );
	//			}
	//		}
	//	}
	//
	//	if ( 0 < count( $value ) ) {
	//
	//		printf(
	//			'<div><strong>Focus:</strong> %s</div>',
	//			implode( ', ', $value )
	//		);
	//	}
	//}

	$terms = $entry->getCategory( array( 'child_of' => 126 ) );

	if ( ! is_array( $terms ) || empty( $terms ) ) {

		return;
	}

	$terms = wp_list_pluck( $terms, 'name', 'term_id' );

	printf(
		'<div><strong>Focus:</strong> %s</div>',
		implode( ', ', $terms )
	);
}

/**
 * @param cnOutput $entry
 */
function renderAudience( $entry ) {

	//$meta = cnMeta::get( 'entry', $entry->getId(), 'audience', TRUE );
	//$data = Connections_QGear::audienceOptions( $meta );
	//
	//if ( is_string( $data ) && 0 < strlen( $data ) ) {
	//
	//	printf(
	//		'<div><strong>Audience:</strong> %s</div>',
	//		$data
	//	);
	//}

	$terms = $entry->getCategory( array( 'child_of' => 140 ) );

	if ( ! is_array( $terms ) || empty( $terms ) ) {

		return;
	}

	$terms = wp_list_pluck( $terms, 'name', 'term_id' );

	printf(
		'<div><strong>Audience:</strong> %s</div>',
		implode( ', ', $terms )
	);
}

/**
 * @param cnOutput $entry
 */
function renderSubscription( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'subscription', TRUE );
	$data = Connections_QGear::subscriptionOptions( $meta );

	if ( is_string( $data ) && 0 < strlen( $data ) ) {

		printf(
			'<div><strong>Subscription Required:</strong> %s</div>',
			$data
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderUploadFrequency( $entry ) {

	//$meta = cnMeta::get( 'entry', $entry->getId(), 'frequency', TRUE );
	//$data = Connections_QGear::frequencyOptions( $meta );
	//
	//if ( is_string( $data ) && 0 < strlen( $data ) ) {
	//
	//	printf(
	//		'<div><strong>Upload Frequency:</strong> %s</div>',
	//		$data
	//	);
	//}

	$terms = $entry->getCategory( array( 'child_of' => 146 ) );

	if ( ! is_array( $terms ) || empty( $terms ) ) {

		return;
	}

	$terms = wp_list_pluck( $terms, 'name', 'term_id' );

	printf(
		'<div><strong>Upload Frequency:</strong> %s</div>',
		implode( ', ', $terms )
	);
}

/**
 * @param cnOutput $entry
 */
function renderVideoLength( $entry ) {

	//$meta = cnMeta::get( 'entry', $entry->getId(), 'video_length', TRUE );
	//$data = Connections_QGear::videoLengthOptions( $meta );
	//
	//if ( is_string( $data ) && 0 < strlen( $data ) ) {
	//
	//	printf(
	//		'<div><strong>Average Video Length:</strong> %s</div>',
	//		$data
	//	);
	//}

	$terms = $entry->getCategory( array( 'child_of' => 151 ) );

	if ( ! is_array( $terms ) || empty( $terms ) ) {

		return;
	}

	$terms = wp_list_pluck( $terms, 'name', 'term_id' );

	printf(
		'<div><strong>Average Video Length:</strong> %s</div>',
		implode( ', ', $terms )
	);
}

/*
 * Audio Icons
 */

/**
 * @param cnOutput $entry
 */
function renderiTunes( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'itunes', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-itunes"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderSoundCloud( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'soundcloud', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-soundcloud"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderPodbean( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'podbean', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-podbean"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderMixlr( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'mixlr', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-mixlr"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/*
 * Financial Support Icons
 */

function renderDonorBox( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'donor-box', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-donor-box"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderGoFundMe( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'go-fund-me', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-go-fund-me"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderPatreon( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'patreon', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-patreon"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderPayPal( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'paypal', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-paypal"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderSubscribestar( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'subscribestar', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-subscribestar"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderWebStore( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'web-store', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-web-store"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/*
 * Text/Post Icons
 */

function renderBlogspot( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'blogspot', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-blogspot"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderDiscord( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'discord', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-discord"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderFacebook( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'facebook', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-facebook"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderGab( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'gab', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-gab"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderInstagram( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'instagram', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-instagram"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderMega( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'mega', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-mega"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderMinds( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'minds', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-minds"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderParler( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'parler', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-parler"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderPinterest( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'pinterest', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-pinterest"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderReddit( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'reddit', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-reddit"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderSteemit( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'steemit', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-steemit"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderTwitter( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'twitter', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-twitter"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

function renderTwitterAlternate( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'twitter-alternate', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-twitter"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/*
 * Video Icons
 */

/**
 * @param cnOutput $entry
 */
function renderAmazonPrime( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'amazon_prime', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-amazon-prime"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderBitchute( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'bitchute', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-bitchute"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderBrighteon( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'brighteon', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-brighteon"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderDLive( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'dlive', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-dlive"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderDropspace( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'dropspace', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-dropspace"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderDtube( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'dtube', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-dtube"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderLBRYU( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'lbryu', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-lbryu"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderPeriscope( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'periscope', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-periscope"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderTwitch( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'twitch', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-twitch"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderUGETube( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'ugetube', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-ugetube"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderVimeo( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'vimeo', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-vimeo"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderYouTubeMain( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'youtube-main|url', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-youtube"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderYouTubeBackup( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'youtube-backup|url', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-youtube"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}

/**
 * @param cnOutput $entry
 */
function renderYouTubeAlternate( $entry ) {

	$meta = cnMeta::get( 'entry', $entry->getId(), 'youtube-alternate|url', TRUE );

	if ( is_string( $meta ) && 0 < strlen( $meta ) ) {

		printf(
			'<span class="cn-icon cn-youtube"><a target="_blank" rel="noopener noreferrer" href="%s"></a></span>',
			$meta
		);
	}
}
