<?php

include_once 'functions.php';

use function Connections_Directory\Template\cMap\Q_Gear\renderAmazonPrime;
use function Connections_Directory\Template\cMap\Q_Gear\renderAudience;
use function Connections_Directory\Template\cMap\Q_Gear\renderBitchute;
use function Connections_Directory\Template\cMap\Q_Gear\renderBlogspot;
use function Connections_Directory\Template\cMap\Q_Gear\renderBrighteon;
use function Connections_Directory\Template\cMap\Q_Gear\renderDiscord;
use function Connections_Directory\Template\cMap\Q_Gear\renderDLive;
use function Connections_Directory\Template\cMap\Q_Gear\renderDonorBox;
use function Connections_Directory\Template\cMap\Q_Gear\renderDropspace;
use function Connections_Directory\Template\cMap\Q_Gear\renderDtube;
use function Connections_Directory\Template\cMap\Q_Gear\renderFacebook;
use function Connections_Directory\Template\cMap\Q_Gear\renderFocus;
use function Connections_Directory\Template\cMap\Q_Gear\renderGab;
use function Connections_Directory\Template\cMap\Q_Gear\renderGoFundMe;
use function Connections_Directory\Template\cMap\Q_Gear\renderInstagram;
use function Connections_Directory\Template\cMap\Q_Gear\renderiTunes;
use function Connections_Directory\Template\cMap\Q_Gear\renderLBRYU;
use function Connections_Directory\Template\cMap\Q_Gear\renderMega;
use function Connections_Directory\Template\cMap\Q_Gear\renderMinds;
use function Connections_Directory\Template\cMap\Q_Gear\renderMixlr;
use function Connections_Directory\Template\cMap\Q_Gear\renderParler;
use function Connections_Directory\Template\cMap\Q_Gear\renderParlerName;
use function Connections_Directory\Template\cMap\Q_Gear\renderPatreon;
use function Connections_Directory\Template\cMap\Q_Gear\renderPayPal;
use function Connections_Directory\Template\cMap\Q_Gear\renderPeriscope;
use function Connections_Directory\Template\cMap\Q_Gear\renderPinterest;
use function Connections_Directory\Template\cMap\Q_Gear\renderPodbean;
use function Connections_Directory\Template\cMap\Q_Gear\renderQStance;
use function Connections_Directory\Template\cMap\Q_Gear\renderReddit;
use function Connections_Directory\Template\cMap\Q_Gear\renderSoundCloud;
use function Connections_Directory\Template\cMap\Q_Gear\renderSteemit;
use function Connections_Directory\Template\cMap\Q_Gear\renderSubscribestar;
use function Connections_Directory\Template\cMap\Q_Gear\renderSubscription;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitch;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitter;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitterAlternate;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitterHandle;
use function Connections_Directory\Template\cMap\Q_Gear\renderUGETube;
use function Connections_Directory\Template\cMap\Q_Gear\renderUploadFrequency;
use function Connections_Directory\Template\cMap\Q_Gear\renderVideoLength;
use function Connections_Directory\Template\cMap\Q_Gear\renderVimeo;
use function Connections_Directory\Template\cMap\Q_Gear\renderWebStore;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeAlternate;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeBackup;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeMain;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeName;

/** @var array $atts */
/** @var cnOutput $entry */
?>
<div id="entry-id-<?php echo $entry->getRuid(); ?>" class="cn-entry-single">

	<div style="display: flex;">
	<div class="cn-left">

		<div style="margin-bottom: 10px;">
			<h3><?php $entry->getNameBlock( array( 'format' => $atts['name_format'], 'link' => FALSE ) ); ?></h3>
			<?php

			if ( $atts['show_title'] ) $entry->getTitleBlock();

			if ( $atts['show_org'] || $atts['show_dept'] ) {

				$entry->getOrgUnitBlock(
					array(
						'show_org'  => $atts['show_org'],
						'show_dept' => $atts['show_dept'],
					)
				);
			}

			if ( $atts['show_contact_name'] ) {

				$entry->getContactNameBlock(
					array(
						'format' => $atts['contact_name_format'],
						'label'  => $atts['str_contact']
					)
				);
			}

			renderTwitterHandle( $entry );
			renderYouTubeName( $entry );
			renderParlerName( $entry );
			renderQStance( $entry );
			renderFocus( $entry );
			renderAudience( $entry );
			renderSubscription( $entry );
			renderUploadFrequency( $entry );
			renderVideoLength( $entry );

			?>
		</div>

		<div style="margin-bottom: 10px;">
			<?php
			if ( $atts['show_addresses'] ) $entry->getAddressBlock( array( 'format' => $atts['addr_format'] , 'type' => $atts['address_types'] ) );

			if ( $atts['show_phone_numbers'] ) $entry->getPhoneNumberBlock( array( 'format' => $atts['phone_format'] , 'type' => $atts['phone_types'] ) );

			if ( $atts['show_email'] ) $entry->getEmailAddressBlock( array( 'format' => $atts['email_format'] , 'type' => $atts['email_types'] ) );

			if ( $atts['show_im'] ) $entry->getImBlock();

			if ( $atts['show_social_media'] ) $entry->getSocialMediaBlock();

			if ( $atts['show_dates'] ) $entry->getDateBlock( array( 'format' => $atts['date_format'], 'type' => $atts['date_types'] ) );

			echo '<div>';
			renderiTunes( $entry );
			renderSoundCloud( $entry );
			renderPodbean( $entry );
			renderMixlr( $entry );
			//echo '</div>';

			//echo '<div>';
			renderAmazonPrime( $entry );
			renderBitchute( $entry );
			renderBrighteon( $entry );
			renderDLive( $entry );
			renderDropspace( $entry );
			renderDtube( $entry );
			renderLBRYU( $entry );
			renderPeriscope( $entry );
			renderTwitch( $entry );
			renderUGETube( $entry );
			renderVimeo( $entry );
			renderYouTubeMain( $entry );
			renderYouTubeBackup( $entry );
			renderYouTubeAlternate( $entry );
			//echo '</div>';

			//echo '<div>';
			renderBlogspot( $entry );
			renderDiscord( $entry );
			renderFacebook( $entry );
			renderGab( $entry );
			renderInstagram( $entry );
			renderMega( $entry );
			renderMinds( $entry );
			renderParler( $entry );
			renderPinterest( $entry );
			renderReddit( $entry );
			renderSteemit( $entry );
			renderTwitter( $entry );
			renderTwitterAlternate( $entry );
			//echo '</div>';

			//echo '<div>';
			renderDonorBox( $entry );
			renderGoFundMe( $entry );
			renderPatreon( $entry );
			renderPayPal( $entry );
			renderSubscribestar( $entry );
			renderWebStore( $entry );
			echo '</div>';

			if ( $atts['show_links'] ) $entry->getLinkBlock( array( 'format' => $atts['link_format'], 'type' => $atts['link_types'] ) );

			if ( $atts['show_family'] )$entry->getFamilyMemberBlock();
			?>
		</div>

	</div>

	<div class="cn-right">

		<?php

		if ( 'none' !== $atts['image'] ) {

			$entry->getImage(
				array(
					'image'    => $atts['image'],
					'width'    => $atts['image_width'],
					'height'   => $atts['image_height'],
					'zc'       => $atts['image_crop_mode'],
					'fallback' => array(
						'type'   => $atts['image_fallback'],
						'string' => $atts['str_image'],
					),
				)
			);

		}

		?>

	</div>
	</div>

	<div class="cn-clear"></div>

	<?php

	if ( $atts['enable_bio'] && 0 < strlen( $entry->getBio() ) ) {

		echo '<div class="cn-bio-single">';

			if ( $atts['enable_bio_head'] ) echo '<h4>' , $atts['str_bio_head'] , '</h4>';

			if ( 'none' !== $atts['tray_image'] ) {
				$entry->getImage(
					array(
						'image'    => $atts['tray_image'],
						'height'   => $atts['tray_image_height'],
						'width'    => $atts['tray_image_width'],
						'fallback' => array(
							'type'   => $atts['tray_image_fallback'],
							'string' => $atts['str_tray_image']
						)
					)
				);
			}

			$entry->getBioBlock();

			echo '<div class="cn-clear"></div>';

		echo '</div>';
	}

	if ( $atts['enable_note'] && 0 < strlen( $entry->getNotes() ) ) {

		echo '<div class="cn-notes-single">';

			if ( $atts['enable_note_head'] ) echo '<h4>' , $atts['str_note_head'] , '</h4>';

			$entry->getNotesBlock();

			echo '<div class="cn-clear"></div>';

		echo '</div>';
	}

	if ( $atts['enable_map'] ) {

		$gMap = $entry->getMapBlock(
			array(
				'height' => $atts['map_frame_height'],
				'width'  => ( $atts['map_frame_width'] ) ? $atts['map_frame_width'] : NULL,
				'return' => TRUE,
				'zoom'   => $atts['map_zoom'],
			)
		);

		if ( ! empty( $gMap ) ) {

			$mapDiv = '<div class="cn-gmap-single" id="cn-gmap-single" data-uuid="' . $entry->getRuid() . '">' . $gMap . '</div>';
		}

	}

	if ( isset( $mapDiv ) ) {

		echo $mapDiv;
	}

	$entry->getContentBlock( $atts['content'], $atts, $template );

	?>

	<div class="cn-clear" style="display:table;margin: 10px 0;width:100%;">
		<div style="display:table-cell;vertical-align:middle;">
			<?php
			if ( $atts['show_categories'] ) {

				$entry->getCategoryBlock(
					array(
						'separator' => ', ',
						'label'     => $atts['str_category_label'],
					)
				);
			}
			?>
		</div>
		<div style="display:table-cell;text-align:right;vertical-align:middle;">
			<?php
			if ( $atts['show_last_updated'] ) {

				cnTemplatePart::updated(
					array(
						'timestamp' => $entry->getUnixTimeStamp(),
						'style'     => array(
							'font-variant' => 'small-caps',
						)
					)
				);
			}
			?>
		</div>
	</div>
</div>
