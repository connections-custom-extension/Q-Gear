<?php

include_once 'functions.php';

use function Connections_Directory\Template\cMap\Q_Gear\renderAmazonPrime;
use function Connections_Directory\Template\cMap\Q_Gear\renderAudience;
use function Connections_Directory\Template\cMap\Q_Gear\renderBitchute;
use function Connections_Directory\Template\cMap\Q_Gear\renderBlogspot;
use function Connections_Directory\Template\cMap\Q_Gear\renderBrighteon;
use function Connections_Directory\Template\cMap\Q_Gear\renderDiscord;
use function Connections_Directory\Template\cMap\Q_Gear\renderDLive;
use function Connections_Directory\Template\cMap\Q_Gear\renderDonorBox;
use function Connections_Directory\Template\cMap\Q_Gear\renderDropspace;
use function Connections_Directory\Template\cMap\Q_Gear\renderDtube;
use function Connections_Directory\Template\cMap\Q_Gear\renderFacebook;
use function Connections_Directory\Template\cMap\Q_Gear\renderFocus;
use function Connections_Directory\Template\cMap\Q_Gear\renderGab;
use function Connections_Directory\Template\cMap\Q_Gear\renderGoFundMe;
use function Connections_Directory\Template\cMap\Q_Gear\renderInstagram;
use function Connections_Directory\Template\cMap\Q_Gear\renderiTunes;
use function Connections_Directory\Template\cMap\Q_Gear\renderLBRYU;
use function Connections_Directory\Template\cMap\Q_Gear\renderMega;
use function Connections_Directory\Template\cMap\Q_Gear\renderMinds;
use function Connections_Directory\Template\cMap\Q_Gear\renderMixlr;
use function Connections_Directory\Template\cMap\Q_Gear\renderParler;
use function Connections_Directory\Template\cMap\Q_Gear\renderParlerName;
use function Connections_Directory\Template\cMap\Q_Gear\renderPatreon;
use function Connections_Directory\Template\cMap\Q_Gear\renderPayPal;
use function Connections_Directory\Template\cMap\Q_Gear\renderPeriscope;
use function Connections_Directory\Template\cMap\Q_Gear\renderPinterest;
use function Connections_Directory\Template\cMap\Q_Gear\renderPodbean;
use function Connections_Directory\Template\cMap\Q_Gear\renderQStance;
use function Connections_Directory\Template\cMap\Q_Gear\renderReddit;
use function Connections_Directory\Template\cMap\Q_Gear\renderSoundCloud;
use function Connections_Directory\Template\cMap\Q_Gear\renderSteemit;
use function Connections_Directory\Template\cMap\Q_Gear\renderSubscribestar;
use function Connections_Directory\Template\cMap\Q_Gear\renderSubscription;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitch;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitter;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitterAlternate;
use function Connections_Directory\Template\cMap\Q_Gear\renderTwitterHandle;
use function Connections_Directory\Template\cMap\Q_Gear\renderUGETube;
use function Connections_Directory\Template\cMap\Q_Gear\renderUploadFrequency;
use function Connections_Directory\Template\cMap\Q_Gear\renderVideoLength;
use function Connections_Directory\Template\cMap\Q_Gear\renderVimeo;
use function Connections_Directory\Template\cMap\Q_Gear\renderWebStore;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeAlternate;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeBackup;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeMain;
use function Connections_Directory\Template\cMap\Q_Gear\renderYouTubeName;

/** @var cnOutput $entry */
/** @var array $atts */
/** @var array $style */
/** @var cnTemplate $template */

$style  = array(
	'background-color' => $atts['background_color'],
	'border'           => $atts['border_width'] . 'px solid ' . $atts['border_color'],
	'border-radius'    => $atts['border_radius'] . 'px',
	//'color'            => '#000',
	'margin'           => '8px 0',
	'padding'          => '10px',
	'position'         => 'relative',
);

/** @var array $class */
$class = array( 'cn-entry' );

if ( $atts['background_gradient'] ) {

	$class[] = 'cn-background-gradient';
}

if ( $atts['background_shadow'] ) {

	$class[] = 'cn-background-shadow';
}

?>
<div id="entry-id-<?php echo $entry->getRuid(); ?>" <?php echo cnHTML::attribute( 'class', $class ); ?> <?php echo cnHTML::attribute( 'style', $style ); ?>>

	<div style="display: flex;">
	<div class="cn-left" style="display: inline-block; vertical-align: top;">

		<?php

		if ( 'none' !== $atts['image'] ) {

			$entry->getImage(
				array(
					'image'    => $atts['image'],
					'width'    => $atts['image_width'],
					'height'   => $atts['image_height'],
					'zc'       => $atts['image_crop_mode'],
					'fallback' => array(
						'type'   => $atts['image_fallback'],
						'string' => $atts['str_image'],
					),
					'permalink' => cnSettingsAPI::get( 'connections', 'connections_link', 'name' ),
				)
			);

		}

		?>
	</div> <!-- /.cn-left -->

	<div class="cn-right" style="display: inline-block; text-align: left; vertical-align: top;">

		<div style="margin: 0 0 5px 16px;">

			<h3><?php $entry->getNameBlock( array( 'format' => $atts['name_format'], 'link' => $atts['link'] ) ); ?></h3>
			<?php
			if ( $atts['show_title'] ) $entry->getTitleBlock();

			if ( $atts['show_org'] || $atts['show_dept'] ) {

				$entry->getOrgUnitBlock(
					array(
						'show_org'  => $atts['show_org'],
						'show_dept' => $atts['show_dept'],
					)
				);
			}

			if ( $atts['show_contact_name'] ) {

				$entry->getContactNameBlock(
					array(
						'format' => $atts['contact_name_format'],
						'label'  => $atts['str_contact']
					)
				);
			}

			renderTwitterHandle( $entry );
			renderYouTubeName( $entry );
			renderParlerName( $entry );
			renderQStance( $entry );
			renderFocus( $entry );
			renderAudience( $entry );
			renderSubscription( $entry );
			renderUploadFrequency( $entry );
			renderVideoLength( $entry );
			?>

		<?php

		if ( $atts['show_addresses'] ) $entry->getAddressBlock( array( 'format' => $atts['addr_format'] , 'type' => $atts['address_types'] ) );

		if ( $atts['show_phone_numbers'] ) $entry->getPhoneNumberBlock( array( 'format' => $atts['phone_format'] , 'type' => $atts['phone_types'] ) );

		if ( $atts['show_email'] ) $entry->getEmailAddressBlock( array( 'format' => $atts['email_format'] , 'type' => $atts['email_types'] ) );

		if ( $atts['show_im'] ) $entry->getImBlock();

		if ( $atts['show_social_media'] ) $entry->getSocialMediaBlock();

		if ( $atts['show_dates'] ) $entry->getDateBlock( array( 'format' => $atts['date_format'], 'type' => $atts['date_types'] ) );

		echo '<div>';
		renderiTunes( $entry );
		renderSoundCloud( $entry );
		renderPodbean( $entry );
		renderMixlr( $entry );
		//echo '</div>';

		//echo '<div>';
		renderAmazonPrime( $entry );
		renderBitchute( $entry );
		renderBrighteon( $entry );
		renderDLive( $entry );
		renderDropspace( $entry );
		renderDtube( $entry );
		renderLBRYU( $entry );
		renderPeriscope( $entry );
		renderTwitch( $entry );
		renderUGETube( $entry );
		renderVimeo( $entry );
		renderYouTubeMain( $entry );
		renderYouTubeBackup( $entry );
		renderYouTubeAlternate( $entry );
		//echo '</div>';

		//echo '<div>';
		renderBlogspot( $entry );
		renderDiscord( $entry );
		renderFacebook( $entry );
		renderGab( $entry );
		renderInstagram( $entry );
		renderMega( $entry );
		renderMinds( $entry );
		renderParler( $entry );
		renderPinterest( $entry );
		renderReddit( $entry );
		renderSteemit( $entry );
		renderTwitter( $entry );
		renderTwitterAlternate( $entry );
		//echo '</div>';

		//echo '<div>';
		renderDonorBox( $entry );
		renderGoFundMe( $entry );
		renderPatreon( $entry );
		renderPayPal( $entry );
		renderSubscribestar( $entry );
		renderWebStore( $entry );
		echo '</div>';

		if ( $atts['show_links'] ) $entry->getLinkBlock( array( 'format' => $atts['link_format'], 'type' => $atts['link_types'] ) );

		if ( $atts['show_family'] )$entry->getFamilyMemberBlock();

		?>
		</div>
	</div><!-- /.cn-right -->
	</div>

	<div class="cn-clear"></div>

	<?php $entry->getContentBlock( $atts['content'], $atts, $template ); ?>

	<div class="cn-clear" style="display:table;margin: 10px 0;width:100%;">
		<div style="display:table-cell;vertical-align:middle;">
			<?php
			if ( $atts['show_categories'] ) {

				$entry->getCategoryBlock(
					array(
						'separator' => ', ',
						'label'     => __( $atts['str_category_label'], 'cnt_cmap' ),
					)
				);
			}
			?>
		</div>
		<div style="display:table-cell;text-align:right;vertical-align:middle;">
			<?php
			if ( $atts['show_last_updated'] ) {

				cnTemplatePart::updated(
					array(
						'timestamp' => $entry->getUnixTimeStamp(),
						'style'     => array(
							'font-variant' => 'small-caps',
						)
					)
				);
			}
			?>
		</div>
	</div>

	<div class="cn-tray-links">

		<div class="cn-left">
			<?php

			if ( $atts['enable_bio'] && 0 < strlen( $entry->getBio() ) ) {

				printf(
					'<a class="cn-bio-anchor cn-cmap-%1$s" id="bio-anchor-%2$s" href="#" data-uuid="%2$s" data-div-id="bio-block-%2$s">%3$s</a>',
					esc_attr( $atts['tray_open_effect'] ),
					$entry->getRuid(),
					$atts['str_bio_show']
				);
			}

			if ( ( $atts['enable_note'] && 0 < strlen( $entry->getNotes() ) ) &&
			     ( $atts['enable_bio'] && 0 < strlen( $entry->getBio() ) ) ) {

				echo '<span class="toggle-bio-note-divider"> | </span>';
			}

			if ( $atts['enable_note'] && 0 < strlen( $entry->getNotes() ) ) {

				printf(
					'<a class="cn-note-anchor cn-cmap-%1$s" id="notes-anchor-%2$s" href="#" data-uuid="%2$s" data-div-id="note-block-%2$s">%3$s</a>',
					esc_attr( $atts['tray_open_effect'] ),
					$entry->getRuid(),
					$atts['str_note_show']
				);
			}

			?>
		</div> <!-- /.cn-left -->

		<div class="cn-right">
			<?php

			if ( $atts['enable_map'] ) {

				$gMap = $entry->getMapBlock(
					array(
						'height' => $atts['map_frame_height'],
						'width'  => ( $atts['map_frame_width'] ) ? $atts['map_frame_width'] : NULL,
						'return' => TRUE,
						'zoom'   => $atts['map_zoom'],
					)
				);

				if ( ! empty( $gMap ) ) {

					$mapDiv = '<div class="cn-cmap-tab cn-gmap" id="map-block-' . $entry->getRuid() . '" data-type="map" style="display: none;">' . $gMap . '</div>';

					printf(
						'<a class="cn-map-anchor cn-cmap-%1$s" id="map-anchor-%2$s" href="#" data-uuid="%2$s" data-div-id="map-block-%2$s">%3$s</a>',
						esc_attr( $atts['tray_open_effect'] ),
						$entry->getRuid(),
						$atts['str_map_show']
					);
				}
			}

			if ( ! empty( $gMap ) && $atts['show_return_to_top'] ) {

				echo '<span class="toggle-map-divider"> | </span>';
			}

			if ( $atts['show_return_to_top'] ) : ?>

				<span class="cn-return-to-top"><?php cnTemplatePart::returnToTop() ?></span>

			<?php endif; ?>

		</div> <!-- /.cn-right -->
	</div> <!-- /.cn-tray-links -->

	<div class="cn-clear"></div>

	<div class="cn-content-tray">
		<?php if ( $atts['enable_bio'] && 0 < strlen( $entry->getBio() ) ) : ?>

			<div class="cn-cmap-tab cn-bio-tray" id="bio-block-<?php echo $entry->getRuid(); ?>" data-type="bio" style="display: none;">

				<?php

				if ( $atts['enable_bio_head'] ) echo '<h4>', $atts['str_bio_head'], '</h4>';

				if ( 'none' !== $atts['tray_image'] ) {

					$entry->getImage(
						array(
							'image'    => $atts['tray_image'],
							'width'    => $atts['tray_image_width'],
							'height'   => $atts['tray_image_height'],
							'zc'       => $atts['tray_image_crop_mode'],
							'fallback' => array(
								'type'   => $atts['tray_image_fallback'],
								'string' => $atts['str_tray_image'],
							),
						)
					);

				}

				$entry->getBioBlock();
				?>

				<div class="cn-clear"></div>

			</div>
		<?php endif; ?>

		<?php if ( $atts['enable_note'] && 0 < strlen( $entry->getNotes() ) ) : ?>

			<div class="cn-cmap-tab cn-note-tray" id="note-block-<?php echo $entry->getRuid(); ?>"  data-type="notes" style="display: none;">

				<?php if ( $atts['enable_note_head'] ) echo '<h4>' , $atts['str_note_head'] , '</h4>'; ?>

				<?php $entry->getNotesBlock(); ?>

				<div class="cn-clear"></div>

			</div>

		<?php endif; ?>

		<?php if ( isset( $mapDiv ) ) echo $mapDiv; ?>
	</div> <!-- /.cn-content-tray -->
</div>
