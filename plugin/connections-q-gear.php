<?php
/**
 * An extension for the Connections Business Directory plugin.
 *
 * @package   Connections Business Directory Extension - Q Gear
 * @category  Extension
 * @author    Steven A. Zahm
 * @license   GPL-2.0+
 * @link      https://connections-pro.com
 * @copyright 2020 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections Business Directory Extension - Q Gear
 * Plugin URI:        https://connections-pro.com/add-on/form/
 * Description:       An extension for the Connections Business Directory plugin for Q gaer.
 * Version:           1.0
 * Author:            Steven A. Zahm
 * Author URI:        https://connections-pro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       connections_q_gear
 * Domain Path:       /languages
 */

if ( ! class_exists( 'Connections_QGear' ) ) {

	final class Connections_QGear {

		const VERSION = '1.0';

		/**
		 * @var Connections_QGear Stores the instance of this class.
		 *
		 * @since 1.0
		 */
		private static $instance;

		/**
		 * @var string The absolute path this this file.
		 *
		 * @since 1.0
		 */
		private $file = '';

		/**
		 * @var string The URL to the plugin's folder.
		 *
		 * @since 1.0
		 */
		private $url = '';

		/**
		 * @var string The absolute path to this plugin's folder.
		 *
		 * @since 1.0
		 */
		private $path = '';

		/**
		 * @var string The basename of the plugin.
		 *
		 * @since 1.0
		 */
		private $basename = '';

		/**
		 * A dummy constructor to prevent the class from being loaded more than once.
		 *
		 * @since 1.0
		 */
		public function __construct() { /* Do nothing here */ }

		/**
		 * The plugin instance.
		 *
		 * @since 1.0
		 *
		 * @return self
		 */
		public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Connections_QGear ) ) {

				self::$instance = $self = new self;

				$self->file     = __FILE__;
				$self->url      = plugin_dir_url( $self->file );
				$self->path     = plugin_dir_path( $self->file );
				$self->basename = plugin_basename( $self->file );

				$self->hooks();

				/**
				 * This should run on the `plugins_loaded` action hook.
				 * Since the extension loads on the `plugins_loaded` action hook, load immediately.
				 */
				cnText_Domain::register(
					'connections_q_gear',
					$self->basename,
					'load'
				);
			}

			return self::$instance;
		}

		/**
		 * @since 1.0
		 */
		private function hooks() {

			// Register custom metaboxes and fields.
			add_action( 'cn_metabox', array( __CLASS__, 'registerFields' ) );

			// Register the field headers for CSV Import/Export
			add_filter( 'cn_csv_export_fields', array( __CLASS__, 'registerCSVHeaders' ) );
			//add_filter( 'cn_export_header-q_stance', array( __CLASS__, 'registerCSVExportQStanceFieldHeader' ), 10, 3 );
			//add_filter( 'cn_export_header-focus', array( __CLASS__, 'registerCSVExportFocusFieldHeader' ), 10, 3 );
			add_filter( 'cn_export_header-specialty', array( __CLASS__, 'registerCSVExportSpecialtyFieldHeader' ), 10, 3 );
			//add_filter( 'cn_export_header-audience', array( __CLASS__, 'registerCSVExportAudienceFieldHeader' ), 10, 3 );
			add_filter( 'cn_export_header-subscription', array( __CLASS__, 'registerCSVExportSubscriptionFieldHeader' ), 10, 3 );
			add_filter( 'cn_export_header-live', array( __CLASS__, 'registerCSVExportLiveFieldHeader' ), 10, 3 );
			add_filter( 'cn_export_header-interactive', array( __CLASS__, 'registerCSVExportInteractiveFieldHeader' ), 10, 3 );
			//add_filter( 'cn_export_header-frequency', array( __CLASS__, 'registerCSVExportFrequencyFieldHeader' ), 10, 3 );
			//add_filter( 'cn_export_header-video_length', array( __CLASS__, 'registerCSVExportVideoLengthFieldHeader' ), 10, 3 );
			add_filter( 'cncsv_map_import_fields', array( __CLASS__, 'registerCSVHeaders' ) );

			// Register the custom fields CSV Export attributes and processing callback.
			add_filter( 'cn_csv_export_fields_config', array( __CLASS__, 'registerCustomFieldCSVExportConfig' ) );
			//add_filter( 'cn_export_field-q_stance', array( __CLASS__, 'registerCustomFieldExportQStance' ), 10, 4 );
			//add_filter( 'cn_export_field-focus', array( __CLASS__, 'registerCustomFieldExportFocus' ), 10, 4 );
			add_filter( 'cn_export_field-specialty', array( __CLASS__, 'registerCustomFieldExportSpecialty' ), 10, 4 );
			//add_filter( 'cn_export_field-audience', array( __CLASS__, 'registerCustomFieldExportAudience' ), 10, 4 );
			add_filter( 'cn_export_field-subscription', array( __CLASS__, 'registerCustomFieldExportSubscription' ), 10, 4 );
			add_filter( 'cn_export_field-live', array( __CLASS__, 'registerCustomFieldExportLive' ), 10, 4 );
			add_filter( 'cn_export_field-interactive', array( __CLASS__, 'registerCustomFieldExportInteractive' ), 10, 4 );
			//add_filter( 'cn_export_field-frequency', array( __CLASS__, 'registerCustomFieldExportFrequency' ), 10, 4 );
			//add_filter( 'cn_export_field-video_length', array( __CLASS__, 'registerCustomFieldExportVideoLength' ), 10, 4 );
			add_action( 'cncsv_import_fields', array( __CLASS__, 'registerCustomFieldImportAction' ), 10, 3 );

			//// Register custom taxonomies admin menu items.
			//add_filter( 'cn_submenu', array( __CLASS__, 'customTaxonomyAdminMenuItems' ) );
			//
			//// Remove the "View" link from the "Focus" & "Specialty" taxonomy admin page.
			//add_filter( 'cn_focus_row_actions', array( __CLASS__, 'removeViewAction' ) );
			//add_filter( 'cn_specialty_row_actions', array( __CLASS__, 'removeViewAction' ) );
			//
			//// Attach custom taxonomies to entry when saving an entry.
			//add_action( 'cn_process_taxonomy-category', array( __CLASS__, 'attachCustomTaxonomies' ), 9, 2 );
		}

		/**
		 * Callback for the `cn_metabox` action.
		 *
		 * @since 1.0
		 */
		public static function registerFields() {

			$atts = array(
				'title'    => 'Q Gear', // Change this to a name which applies to your project.
				'id'       => 'q_gear', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					//array(
					//	// Change this field name to something which applies to you project.
					//	'name'       => 'Q Stance',
					//	// Whether or not to display the 'name'. Changing it to false will suppress the name.
					//	'show_label' => true,
					//	// Change this so it is unique to you project. Each field id MUST be unique.
					//	'id'         => 'q_stance',
					//	// This is the field type being added.
					//	'type'       => 'radio',
					//	'options'    => self::qStanceOptions(),
					//	// This is the default selected option. Leave blank for none.
					//	'default'    => '',
					//),
					//array(
					//	// Change this field name to something which applies to you project.
					//	'name'       => 'Focus',
					//	// Whether or not to display the 'name'. Changing it to false will suppress the name.
					//	'show_label' => true,
					//	// Change this so it is unique to you project. Each field id MUST be unique.
					//	'id'         => 'focus',
					//	// This is the field type being added.
					//	'type'       => 'checkboxgroup',
					//	'options'    => self::focusOptions(),
					//	// This is the default selected option. Leave blank for none.
					//	'default'    => '',
					//),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Specialty',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'specialty',
						// This is the field type being added.
						'type'       => 'checkboxgroup',
						'options'    => self::specialtyOptions(),
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					//array(
					//	// Change this field name to something which applies to you project.
					//	'name'       => 'Audience',
					//	// Whether or not to display the 'name'. Changing it to false will suppress the name.
					//	'show_label' => true,
					//	// Change this so it is unique to you project. Each field id MUST be unique.
					//	'id'         => 'audience',
					//	// This is the field type being added.
					//	'type'       => 'radio',
					//	'options'    => self::audienceOptions(),
					//	// This is the default selected option. Leave blank for none.
					//	'default'    => '',
					//),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Subscription',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'subscription',
						// This is the field type being added.
						'type'       => 'radio',
						'options'    => self::subscriptionOptions(),
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Audio', // Change this to a name which applies to your project.
				'id'       => 'audio', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'iTunes',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'itunes',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'SoundCloud',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'soundcloud',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Podbean',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'podbean',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Mixlr',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'mixlr',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Video', // Change this to a name which applies to your project.
				'id'       => 'video', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Amazon Prime',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'amazon_prime',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Bitchute',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'bitchute',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Brighteon',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'brighteon',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'DLive',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'dlive',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Dropspace',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'dropspace',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'DTube',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'dtube',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'LBRYU',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'lbryu',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Periscope',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'periscope',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Twitch',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'twitch',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'UGETube',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'ugetube',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Vimeo',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'vimeo',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'YouTube Main Channel Name',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'youtube-main|name',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'YouTube Main URL',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'youtube-main|url',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'YouTube Backup URL',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'youtube-backup|url',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'YouTube Alternate URL',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'youtube-alternate|url',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Text/Post', // Change this to a name which applies to your project.
				'id'       => 'text_post', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Blogspot',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'blogspot',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Discord',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'discord',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Facebook',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'facebook',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Gab',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'gab',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Instagram',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'instagram',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Mega',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'mega',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Minds',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'minds',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Parler',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'parler',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Pinterest',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'pinterest',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Reddit',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'reddit',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Steemit',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'steemit',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Twitter',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'twitter',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Twitter Alternate',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'twitter-alternate',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Financial Support', // Change this to a name which applies to your project.
				'id'       => 'financial_support', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Donor Box',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'donor-box',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Go Fund Me',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'go-fund-me',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Patreon',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'patreon',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'PayPal',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'paypal',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Subscribestar',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'subscribestar',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Web Store',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'web-store',
						// This is the field type being added.
						'type'       => 'text',
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Video Posting', // Change this to a name which applies to your project.
				'id'       => 'video-posting', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Live Audio/Video',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'live',
						// This is the field type being added.
						'type'       => 'radio',
						'options'    => self::liveOptions(),
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					array(
						// Change this field name to something which applies to you project.
						'name'       => 'Interactive',
						// Whether or not to display the 'name'. Changing it to false will suppress the name.
						'show_label' => true,
						// Change this so it is unique to you project. Each field id MUST be unique.
						'id'         => 'interactive',
						// This is the field type being added.
						'type'       => 'radio',
						'options'    => self::interactiveOptions(),
						// This is the default selected option. Leave blank for none.
						'default'    => '',
					),
					//array(
					//	// Change this field name to something which applies to you project.
					//	'name'       => 'Frequency',
					//	// Whether or not to display the 'name'. Changing it to false will suppress the name.
					//	'show_label' => true,
					//	// Change this so it is unique to you project. Each field id MUST be unique.
					//	'id'         => 'frequency',
					//	// This is the field type being added.
					//	'type'       => 'radio',
					//	'options'    => self::frequencyOptions(),
					//	// This is the default selected option. Leave blank for none.
					//	'default'    => '',
					//),
					//array(
					//	// Change this field name to something which applies to you project.
					//	'name'       => 'Video Length',
					//	// Whether or not to display the 'name'. Changing it to false will suppress the name.
					//	'show_label' => true,
					//	// Change this so it is unique to you project. Each field id MUST be unique.
					//	'id'         => 'video_length',
					//	// This is the field type being added.
					//	'type'       => 'radio',
					//	'options'    => self::videoLengthOptions(),
					//	// This is the default selected option. Leave blank for none.
					//	'default'    => '',
					//),
				),
			);

			cnMetaboxAPI::add( $atts );

			//$atts = array(
			//	'id'       => 'taxonomy-focuses',
			//	'title'    => __( 'Focuses', 'connections_q_gear' ),
			//	//'pages'    => $pages,
			//	'context'  => 'side',
			//	'priority' => 'core',
			//	'callback' => array( __CLASS__, 'metaboxFocuses' ),
			//);
			//
			//cnMetaboxAPI::add( $atts );
			//
			//$atts = array(
			//	'id'       => 'taxonomy-specialties',
			//	'title'    => __( 'Specialties', 'connections_q_gear' ),
			//	//'pages'    => $pages,
			//	'context'  => 'side',
			//	'priority' => 'core',
			//	'callback' => array( __CLASS__, 'metaboxSpecialties' ),
			//);
			//
			//cnMetaboxAPI::add( $atts );
		}

		/**
		 * @since 1.0
		 *
		 * @param string|null $key
		 *
		 * @return array|bool|string
		 */
		public static function qStanceOptions( $key = null ) {

			$value   = false;
			$options = array(
				'pro-q'     => 'Pro Q',
				'affirms_q' => 'Affirms Q',
				'neutral_q' => 'Neutral Q',
				'non_q'     => 'Non Q',
				'not_pro_q' => 'Not Pro Q',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		public static function focusOptions( $key = null ) {

			$value   = false;
			$options = array(
				'political'       => 'Political',
				'political-world' => 'Political - World/Geopolitical',
				'political-us'    => 'Policical - U.S.',
				'political-gb'    => 'Political - UK',
				'political-ca'    => 'Political - Canada',
				'political-cn'    => 'Political - China',
				'political-ru'    => 'Political - Russia',
				'political-eu'    => 'Political - EU',
				'political-other' => 'Political - Other',
				'spiritual'       => 'Spiritual',
				'health'          => 'Health',
				'financial'       => 'Financial',
				'historical'      => 'Historical',
				'music'           => 'Music',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		private static function specialtyOptions( $key = null ) {

			$value   = false;
			$options = array(
				'q_drop_decoder' => 'Q Drop Decoder',
				'analysis'       => 'Analysis',
				'opinion'        => 'Opinion',
				'investigator'   => 'Investigator',
				'gematria'       => 'Gematria',
				'satire'         => 'Satire',
				'other'          => 'Other',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		public static function audienceOptions( $key = null ) {

			$value   = false;
			$options = array(
				'under_10k' => 'Under 10K',
				'10k_50k'   => '10K - 50K',
				'50k_250k'  => '50K - 250K',
				'250k_1m'   => '250K - 1M',
				'over_1m'   => 'Over 1M',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		public static function subscriptionOptions( $key = null ) {

			$value   = false;
			$options = array(
				'no'   => 'No',
				'some' => 'Yes: some content',
				'most' => 'Yes: most content',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		private static function liveOptions( $key = null ) {

			$value   = false;
			$options = array(
				'some' => 'Some Shows',
				'all'  => 'All Shows',
				'none' => 'None',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		private static function interactiveOptions( $key = null ) {

			$value   = false;
			$options = array(
				'call' => 'Phone/Video Calls',
				'live' => 'Takes live Questions',
				'no'   => 'No',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		public static function frequencyOptions( $key = null ) {

			$value   = false;
			$options = array(
				'call'        => 'Daily',
				'weekly_3+'   => 'Weekly 3+',
				'weekly_1-2'  => 'Weekly 1-2',
				'monthly_1-3' => 'Monthly 1-3',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * @since 1.0
		 *
		 * @param string $key
		 *
		 * @return array|bool|string
		 */
		public static function videoLengthOptions( $key = null ) {

			$value   = false;
			$options = array(
				'short'  => 'Short (0-10 min)',
				'medium' => 'Medium (10-30 min)',
				'long'   => 'Long (30-120 min)',
				'xlong'  => 'Extra Long (120 min +)',
			);

			if ( is_null( $key ) ) {

				$value = $options;

			} elseif ( array_key_exists( $key, $options ) ) {

				$value = $options[ $key ];
			}

			return $value;
		}

		/**
		 * Callback for the `cn_submenu` filter.
		 *
		 * @param array $menu
		 *
		 * @return array
		 */
		public static function customTaxonomyAdminMenuItems( $menu ) {

			$menu[62]  = array(
				'hook'       => 'focus',
				'page_title' => 'Connections : ' . __( 'Focus', 'connections_q_gear' ),
				'menu_title' => __( 'Focus', 'connections_q_gear' ),
				'capability' => 'connections_edit_categories',
				'menu_slug'  => 'connections_focus',
				'function'   => array( __CLASS__, 'viewTaxonomyAdminPage' ),
			);

			$menu[66]  = array(
				'hook'       => 'specialty',
				'page_title' => 'Connections : ' . __( 'Specialty', 'connections_q_gear' ),
				'menu_title' => __( 'Specialty', 'connections_q_gear' ),
				'capability' => 'connections_edit_categories',
				'menu_slug'  => 'connections_specialty',
				'function'   => array( __CLASS__, 'viewTaxonomyAdminPage' ),
			);

			return $menu;
		}

		public static function viewTaxonomyAdminPage() {

			// Grab an instance of the Connections object.
			$directory = Connections_Directory();
			$instance  = Connections_QGear();

			if ( $directory->dbUpgrade ) {

				include_once CN_PATH . 'includes/inc.upgrade.php';
				connectionsShowUpgradePage();
				return;
			}

			include_once $instance->path . 'includes/admin/pages/custom-taxonomy.php';

			switch ( $_GET['page'] ) {

				case 'connections_focus':

					$args = array(
						'labels' => array(
							'name'              => _x( 'Focuses', 'taxonomy general name', 'connections_q_gear' ),
							'singular_name'     => _x( 'Focus', 'taxonomy singular name', 'connections_q_gear' ),
							'search_items'      => __( 'Search Focuses', 'connections_q_gear' ),
							'all_items'         => __( 'All Focuses', 'connections_q_gear' ),
							'parent_item'       => __( 'Parent Focus', 'connections_q_gear' ),
							'parent_item_colon' => __( 'Parent Focus:', 'connections_q_gear' ),
							'edit_item'         => __( 'Edit Focus', 'connections_q_gear' ),
							'update_item'       => __( 'Update Focus', 'connections_q_gear' ),
							'add_new_item'      => __( 'Add New Focus', 'connections_q_gear' ),
							'new_item_name'     => __( 'New Focus Name', 'connections_q_gear' ),
						),
					);

					qGearCustomTaxonomyAdmin( 'focus', $args );
					break;

				case 'connections_specialty':

					$args = array(
						'labels' => array(
							'name'              => _x( 'Specialties', 'taxonomy general name', 'connections_q_gear' ),
							'singular_name'     => _x( 'Specialty', 'taxonomy singular name', 'connections_q_gear' ),
							'search_items'      => __( 'Search Specialties', 'connections_q_gear' ),
							'all_items'         => __( 'All Specialties', 'connections_q_gear' ),
							'parent_item'       => __( 'Parent Specialty', 'connections_q_gear' ),
							'parent_item_colon' => __( 'Parent Specialty:', 'connections_q_gear' ),
							'edit_item'         => __( 'Edit Specialty', 'connections_q_gear' ),
							'update_item'       => __( 'Update Specialty', 'connections_q_gear' ),
							'add_new_item'      => __( 'Add New Specialty', 'connections_q_gear' ),
							'new_item_name'     => __( 'New Specialty Name', 'connections_q_gear' ),
						),
					);

					qGearCustomTaxonomyAdmin( 'specialty', $args );
					break;
			}
		}

		/**
		 * Callback for the `cn_{$Taxonomy}_row_actions` filter.
		 *
		 * @since 1.0
		 *
		 * @param array $actions
		 *
		 * @return array
		 */
		public static function removeViewAction( $actions ) {

			unset( $actions['view'] );

			return $actions;
		}

		/**
		 * The Focuses metabox.
		 *
		 * @since  1.0
		 *
		 * @param  cnEntry $entry   An instance of the cnEntry object.
		 * @param  array   $metabox The metabox options array from self::register().
		 */
		public static function metaboxFocuses( $entry, $metabox ) {

			echo '<div class="focusdiv" id="taxonomy-focus">';

			$style = <<<HEREDOC
<style type="text/css" scoped>
	.focusdiv div.tabs-panel {
		min-height: 42px;
		max-height: 200px;
		overflow: auto;
		padding: 0 0.9em;
		border: solid 1px #ddd;
		background-color: #fdfdfd;
	}
	.focusdiv ul.focuschecklist ul {
		margin-left: 18px;
	}
</style>
HEREDOC;
			echo $style;

			echo '<div id="focus-all" class="tabs-panel">';

			cnTemplatePart::walker(
				'term-checklist',
				array(
					'name'     => 'entry_focus',
					'taxonomy' => 'focus',
					'selected' => cnTerm::getRelationships( $entry->getID(), 'focus', array( 'fields' => 'ids' ) ),
				)
			);

			echo '</div>';
			echo '</div>';
		}

		/**
		 * The specialties metabox.
		 *
		 * @since  1.0
		 *
		 * @param  cnEntry $entry   An instance of the cnEntry object.
		 * @param  array   $metabox The metabox options array from self::register().
		 */
		public static function metaboxSpecialties( $entry, $metabox ) {

			echo '<div class="focusdiv" id="taxonomy-specialty">';

			$style = <<<HEREDOC
<style type="text/css" scoped>
	.specialtydiv div.tabs-panel {
		min-height: 42px;
		max-height: 200px;
		overflow: auto;
		padding: 0 0.9em;
		border: solid 1px #ddd;
		background-color: #fdfdfd;
	}
	.specialtydiv ul.specialtychecklist ul {
		margin-left: 18px;
	}
</style>
HEREDOC;
			echo $style;

			echo '<div id="specialty-all" class="tabs-panel">';

			cnTemplatePart::walker(
				'term-checklist',
				array(
					'name'     => 'entry_specialty',
					'taxonomy' => 'specialty',
					'selected' => cnTerm::getRelationships( $entry->getID(), 'specialty', array( 'fields' => 'ids' ) ),
				)
			);

			echo '</div>';
			echo '</div>';
		}

		/**
		 * Callback for the `cn_process_taxonomy-category` action.
		 *
		 * Add, update or delete the custom taxonomies.
		 *
		 * @since  1.0
		 *
		 * @param  string $action The action to being performed to an entry.
		 * @param  int    $id     The entry ID.
		 */
		public static function attachCustomTaxonomies( $action, $id ) {

			// Grab an instance of the Connections object.
			$instance = Connections_Directory();

			if ( isset( $_POST['entry_focus'] ) && ! empty( $_POST['entry_focus'] ) ) {

				$instance->term->setTermRelationships( $id, $_POST['entry_focus'], 'focus' );

			} else {

				$instance->term->setTermRelationships( $id, array(), 'focus' );
			}

			if ( isset( $_POST['entry_specialty'] ) && ! empty( $_POST['entry_specialty'] ) ) {

				$instance->term->setTermRelationships( $id, $_POST['entry_specialty'], 'specialty' );

			} else {

				$instance->term->setTermRelationships( $id, array(), 'specialty' );
			}
		}

		/**
		 * Callback for the `cn_export_header-q_stance` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportQStanceFieldHeader( $header, $field, $export ) {

			return 'Q Stance';
		}

		/**
		 * Callback for the `cn_export_header-focus` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportFocusFieldHeader( $header, $field, $export ) {

			return 'Focus';
		}

		/**
		 * Callback for the `cn_export_header-specialty` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportSpecialtyFieldHeader( $header, $field, $export ) {

			return 'Specialty';
		}

		/**
		 * Callback for the `cn_export_header-audience` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportAudienceFieldHeader( $header, $field, $export ) {

			return 'Audience';
		}

		/**
		 * Callback for the `cn_export_header-subscription` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportSubscriptionFieldHeader( $header, $field, $export ) {

			return 'Subscription';
		}

		/**
		 * Callback for the `cn_export_header-live` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportLiveFieldHeader( $header, $field, $export ) {

			return 'Live Audio/Video';
		}

		/**
		 * Callback for the `cn_export_header-interactive` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportInteractiveFieldHeader( $header, $field, $export ) {

			return 'Interactive';
		}

		/**
		 * Callback for the `cn_export_header-frequency` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportFrequencyFieldHeader( $header, $field, $export ) {

			return 'Frequency';
		}

		/**
		 * Callback for the `cn_export_header-frequency` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $header
		 * @param array                  $field
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCSVExportVideoLengthFieldHeader( $header, $field, $export ) {

			return 'Video Length';
		}

		/**
		 * Callback for the `cn_csv_export_fields` and `cncsv_map_import_fields` filters
		 *
		 * @since 1.0
		 *
		 * @param array $heading
		 *
		 * @return array
		 */
		public static function registerCSVHeaders( $heading ) {

			//$heading['q_stance']     = 'Q Stance';
			//$heading['focus']        = 'Focus';
			$heading['specialty']    = 'Specialty';
			//$heading['audience']     = 'Audience';
			$heading['subscription'] = 'Subscription';

			$heading['itunes'] = 'iTunes';
			$heading['soundcloud'] = 'SoundCloud';
			$heading['podbean'] = 'Podbean';
			$heading['mixlr'] = 'Mixlr';

			$heading['amazon_prime'] = 'Amazon Prime';
			$heading['bitchute']     = 'Bitchute';
			$heading['brighteon']    = 'Brighteon';
			$heading['dLive']        = 'DLive';
			$heading['dropspace']    = 'Dropspace';
			$heading['dtube']        = 'DTube';
			$heading['lbryu']        = 'LBRYU';
			$heading['periscope']    = 'Periscope';
			$heading['twitch']       = 'Twitch';
			$heading['ugetube']      = 'UGETube';
			$heading['vimeo']        = 'Vimeo';

			$heading['youtube-main|name']     = 'YouTube Main Channel Name';
			$heading['youtube-main|url']      = 'YouTube Main URL';
			$heading['youtube-backup|url']    = 'YouTube Backup URL';
			$heading['youtube-alternate|url'] = 'YouTube Alternate URL';

			$heading['blogspot']          = 'Blogspot';
			$heading['discord']           = 'Discord';
			$heading['facebook']          = 'Facebook';
			$heading['gab']               = 'Gab';
			$heading['instagram']         = 'Instagram';
			$heading['mega']              = 'Mega';
			$heading['minds']             = 'Minds';
			$heading['parler']            = 'Parler';
			$heading['pinterest']         = 'Pinterest';
			$heading['reddit']            = 'Reddit';
			$heading['steemit']           = 'Steemit';
			$heading['twitter']           = 'Twitter';
			$heading['twitter-alternate'] = 'Twitter Alternate';

			$heading['donor-box']     = 'Donor Box';
			$heading['go-fund-me']    = 'Go Fund Me';
			$heading['patreon']       = 'Patreon';
			$heading['paypal']        = 'PayPal';
			$heading['subscribestar'] = 'Subscribestar';
			$heading['web-store']     = 'Web Store';

			$heading['live']         = 'Live Audio/Video';
			$heading['interactive']  = 'Interactive';
			//$heading['frequency']    = 'Frequency';
			//$heading['video_length'] = 'Video Length';

			return $heading;
		}

		/**
		 * Callback for the `cn_csv_export_fields_config` filter.
		 *
		 * @since  1.0
		 *
		 * @param array $fields
		 *
		 * @return array
		 */
		public static function registerCustomFieldCSVExportConfig( $fields ) {

			//$fields[] = array(
			//	'field'  => 'q_stance', // The field_id should match exactly the field id used when registering the custom field.
			//	'type'   => 'q_stance',
			//	'fields' => '',
			//	'table'  => CN_ENTRY_TABLE_META,
			//	'types'  => NULL,
			//);

			//$fields[] = array(
			//	'field'  => 'focus', // The field_id should match exactly the field id used when registering the custom field.
			//	'type'   => 'focus',
			//	'fields' => '',
			//	'table'  => CN_ENTRY_TABLE_META,
			//	'types'  => NULL,
			//);

			$fields[] = array(
				'field'  => 'specialty', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 'specialty',
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			//$fields[] = array(
			//	'field'  => 'audience', // The field_id should match exactly the field id used when registering the custom field.
			//	'type'   => 'audience',
			//	'fields' => '',
			//	'table'  => CN_ENTRY_TABLE_META,
			//	'types'  => NULL,
			//);

			$fields[] = array(
				'field'  => 'subscription', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 'subscription',
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'itunes', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'soundcloud', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'podbean', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'mixlr', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'amazon_prime', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'bitchute', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'brighteon', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'dropspace', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'dtube', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'lbryu', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'periscope', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'twitch', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'ugetube', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'vimeo', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'youtube-main|name', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'youtube-main|url', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'youtube-backup|url', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'youtube-alternate|url', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'blogspot', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'discord', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'facebook', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'gab', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'instagram', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'mega', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'minds', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'parler', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'pinterest', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'reddit', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'steemit', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'twitter', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'twitter-alternate', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'donor-box', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'go-fund-me', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'patreon', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'paypal', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'subscribestar', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'web-store', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'live', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 'live',
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'interactive', // The field_id should match exactly the field id used when registering the custom field.
				'type'   => 'interactive',
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			//$fields[] = array(
			//	'field'  => 'frequency', // The field_id should match exactly the field id used when registering the custom field.
			//	'type'   => 'frequency',
			//	'fields' => '',
			//	'table'  => CN_ENTRY_TABLE_META,
			//	'types'  => NULL,
			//);

			//$fields[] = array(
			//	'field'  => 'video_length', // The field_id should match exactly the field id used when registering the custom field.
			//	'type'   => 'video_length',
			//	'fields' => '',
			//	'table'  => CN_ENTRY_TABLE_META,
			//	'types'  => NULL,
			//);

			return $fields;
		}

		/**
		 * Callback for the `cn_export_field-q_stance` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportQStance( $value, $entry, $field, $export ) {

			if ( 'q_stance' !== $field['field'] ) return $value;

			$value = '';
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data   = cnFormatting::maybeJSONencode( $meta );
				$stance = self::qStanceOptions( $data );

				if ( FALSE !== $stance ) {

					$value = $export->escapeAndQuote( $stance );
				}
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-focus` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportFocus( $value, $entry, $field, $export ) {

			if ( 'focus' !== $field['field'] ) return $value;

			$value = array();
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data = cnFormatting::maybeJSONdecode( $meta );

				if ( is_array( $data ) ) {

					foreach ( $data as $key ) {

						$option = self::focusOptions( $key );

						if ( FALSE !== $option ) {

							array_push( $value, $option );
						}
					}
				}

				$value = $export->escapeAndQuote( implode( ', ', $value ) );
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-specialty` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportSpecialty( $value, $entry, $field, $export ) {

			if ( 'specialty' !== $field['field'] ) return $value;

			$value = array();
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data = cnFormatting::maybeJSONdecode( $meta );

				if ( is_array( $data ) ) {

					foreach ( $data as $key ) {

						$option = self::specialtyOptions( $key );

						if ( FALSE !== $option ) {

							array_push( $value, $option );
						}
					}
				}

				$value = $export->escapeAndQuote( implode( ', ', $value ) );
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-audience` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportAudience( $value, $entry, $field, $export ) {

			if ( 'audience' !== $field['field'] ) return $value;

			$value = '';
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data   = cnFormatting::maybeJSONencode( $meta );
				$stance = self::audienceOptions( $data );

				if ( FALSE !== $stance ) {

					$value = $export->escapeAndQuote( $stance );
				}
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-subscription` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportSubscription( $value, $entry, $field, $export ) {

			if ( 'subscription' !== $field['field'] ) return $value;

			$value = '';
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data   = cnFormatting::maybeJSONencode( $meta );
				$stance = self::subscriptionOptions( $data );

				if ( FALSE !== $stance ) {

					$value = $export->escapeAndQuote( $stance );
				}
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-live` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportLive( $value, $entry, $field, $export ) {

			if ( 'live' !== $field['field'] ) return $value;

			$value = '';
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data   = cnFormatting::maybeJSONencode( $meta );
				$stance = self::liveOptions( $data );

				if ( FALSE !== $stance ) {

					$value = $export->escapeAndQuote( $stance );
				}
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-interactive` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportInteractive( $value, $entry, $field, $export ) {

			if ( 'interactive' !== $field['field'] ) return $value;

			$value = '';
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data   = cnFormatting::maybeJSONencode( $meta );
				$stance = self::interactiveOptions( $data );

				if ( FALSE !== $stance ) {

					$value = $export->escapeAndQuote( $stance );
				}
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-frequency` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportFrequency( $value, $entry, $field, $export ) {

			if ( 'frequency' !== $field['field'] ) return $value;

			$value = '';
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data   = cnFormatting::maybeJSONencode( $meta );
				$stance = self::frequencyOptions( $data );

				if ( FALSE !== $stance ) {

					$value = $export->escapeAndQuote( $stance );
				}
			}

			return $value;
		}

		/**
		 * Callback for the `cn_export_field-video_length` filter.
		 *
		 * @since 1.0
		 *
		 * @param string                 $value
		 * @param object                 $entry
		 * @param array                  $field The field config array.
		 * @param cnCSV_Batch_Export_All $export
		 *
		 * @return string
		 */
		public static function registerCustomFieldExportVideoLength( $value, $entry, $field, $export ) {

			if ( 'video_length' !== $field['field'] ) return $value;

			$value = '';
			$meta  = cnMeta::get( 'entry', $entry->id, $field['field'], TRUE );

			if ( ! empty( $meta ) ) {

				$data   = cnFormatting::maybeJSONencode( $meta );
				$stance = self::videoLengthOptions( $data );

				if ( FALSE !== $stance ) {

					$value = $export->escapeAndQuote( $stance );
				}
			}

			return $value;
		}

		/**
		 * Callback for the `cncsv_import_fields` action.
		 *
		 * @since 1.0
		 *
		 * @param int         $id
		 * @param array       $row
		 * @param cnCSV_Entry $entry
		 */
		public static function registerCustomFieldImportAction( $id, $row, $entry ) {

			$meta = array();

			if ( $entry->arrayKeyExists( $row, 'q_stance' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'q_stance', '' );
				$value = trim( $value );

				if ( false !== $key = array_search( $value, self::qStanceOptions(), true ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'q_stance',
							'value' => $key,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'focus' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'focus', '' );
				$value = explode( ',' , trim( $value ) );
				$data  = array();

				foreach ( $value as $key ) {

					if ( false !== $result = array_search( trim( $key ), self::focusOptions(), true ) ) {

						array_push( $data, $result );
					}
				}

				if ( 0 < count( $data ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'focus',
							'value' => $data,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'specialty' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'specialty', '' );
				$value = explode( ',' , trim( $value ) );
				$data  = array();

				foreach ( $value as $key ) {

					if ( false !== $result = array_search( trim( $key ), self::specialtyOptions(), true ) ) {

						array_push( $data, $result );
					}
				}

				if ( 0 < count( $data ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'specialty',
							'value' => $data,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'audience' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'audience', '' );
				$value = trim( $value );

				if ( false !== $key = array_search( $value, self::audienceOptions(), true ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'audience',
							'value' => $key,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'subscription' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'subscription', '' );
				$value = trim( $value );

				if ( false !== $key = array_search( $value, self::subscriptionOptions(), true ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'subscription',
							'value' => $key,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'itunes' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'itunes', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'itunes',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'soundcloud' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'soundcloud', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'soundcloud',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'podbean' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'podbean', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'podbean',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'mixlr' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'mixlr', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'mixlr',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'amazon_prime' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'amazon_prime', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'amazon_prime',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'bitchute' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'bitchute', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'bitchute',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'brighteon' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'brighteon', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'brighteon',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'dlive' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'dlive', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'dlive',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'dropspace' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'dropspace', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'dropspace',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'dtube' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'dtube', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'dtube',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'lbryu' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'lbryu', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'lbryu',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'periscope' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'periscope', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'periscope',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'twitch' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'twitch', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'twitch',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'ugetube' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'ugetube', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'ugetube',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'vimeo' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'vimeo', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'vimeo',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'youtube-main|name' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'youtube-main|name', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'youtube-main|name',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'youtube-main|url' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'youtube-main|url', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'youtube-main|url',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'youtube-backup|url' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'youtube-backup|url', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'youtube-backup|url',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'youtube-alternate|url' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'youtube-alternate|url', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'youtube-alternate|url',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'blogpost' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'blogpost', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'blogpost',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'blogspot' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'blogspot', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'blogspot',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'discord' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'discord', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'discord',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'facebook' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'facebook', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'facebook',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'gab' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'gab', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'gab',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'instagram' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'instagram', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'instagram',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'mega' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'mega', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'mega',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'minds' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'minds', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'minds',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'parler' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'parler', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'parler',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'pinterest' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'pinterest', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'pinterest',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'reddit' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'reddit', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'reddit',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'steemit' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'steemit', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'steemit',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'twitter' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'twitter', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'twitter',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'twitter-alternate' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'twitter-alternate', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'twitter-alternate',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'donor-box' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'donor-box', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'donor-box',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'go-fund-me' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'go-fund-me', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'go-fund-me',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'patreon' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'patreon', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'patreon',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'paypal' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'paypal', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'paypal',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'subscribestar' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'subscribestar', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'subscribestar',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'web-store' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'web-store', '' );
				$value = trim( $value );

				$meta[] = array(
					'key'   => 'web-store',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'live' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'live', '' );
				$value = trim( $value );

				if ( false !== $key = array_search( $value, self::liveOptions(), true ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'live',
							'value' => $key,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'interactive' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'interactive', '' );
				$value = trim( $value );

				if ( false !== $key = array_search( $value, self::interactiveOptions(), true ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'interactive',
							'value' => $key,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'frequency' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'frequency', '' );
				$value = trim( $value );

				if ( false !== $key = array_search( $value, self::frequencyOptions(), true ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'frequency',
							'value' => $key,
						)
					);
				}
			}

			if ( $entry->arrayKeyExists( $row, 'video_length' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'video_length', '' );
				$value = trim( $value );

				if ( false !== $key = array_search( $value, self::videoLengthOptions(), true ) ) {

					array_push(
						$meta,
						array(
							'key'   => 'video_length',
							'value' => $key,
						)
					);
				}
			}

			if ( 0 < count( $meta ) ) {

				cnEntry_Action::meta( 'update', $id, $meta );
			}
		}
	}

	/**
	 * Start up the extension.
	 *
	 * @since 1.0
	 *
	 * @return Connections_QGear|false
	 */
	function Connections_QGear() {

		if ( class_exists('connectionsLoad') ) {

			return Connections_QGear::instance();

		} else {

			add_action(
				'admin_notices',
				create_function(
					'',
					'echo \'<div id="message" class="error"><p><strong>ERROR:</strong> Connections must be installed and active in order use this extension.</p></div>\';'
				)
			);

			return FALSE;
		}
	}

	/**
	 * We'll load the extension on `plugins_loaded` so we know Connections will be loaded and ready first.
	 */
	add_action( 'plugins_loaded', 'Connections_QGear' );
}
